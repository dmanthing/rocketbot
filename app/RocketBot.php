<?php

namespace RocketBot;

require_once "./vendor/autoload.php";

use React\EventLoop\Factory;
use Slack\RealTimeClient;
use Slack\Message\{Attachment, AttachmentBuilder, AttachmentField};
use Flintstone\Flintstone;

class RocketBot
{
    private $loop;
    private $client;
    private $players;
    private $channel;
    private $channelId;
    private $botId;
    private $teams;
    private $games;
    private $tourneyStatus;
    private $tourneyAdmin;
    private $joinOpen;
    private $db;
    private $dbOptions;
    private $winners;

    public function __construct($botId, $channelId, $token)
    {
        $this->botId = $botId;
        $this->channelId = $channelId;
        $this->loop = Factory::create();
        $this->client = new RealTimeClient($this->loop);
        $this->client->setToken($token);
        $this->client->connect();
        $this->dbOptions = ['dir' => './db'];
        $this->db = new Flintstone('tourneys', $this->dbOptions);
        if($this->db->get('players') != "false")
            $this->players = array();
        else
            $this->players = $this->db->get('players');
        if($this->db->get('teams') != "false")
            $this->teams = $this->db->get('teams');
        else
            $this->teams = array();
        if($this->db->get('status') != "false")
            $this->tourneyStatus = $this->db->get("status");
        else
            $this->tourneyStatus = "inactive";
        if($this->db->get('admin') != "false")
            $this->tourneyAdmin = $this->db->get("admin");
        else
            $this->tourneyAdmin = array();
        if($this->db->get('games') != "false")
            $this->games = $this->db->get("games");
        else
            $this->games = array();   
        if($this->db->get('join') != "false")
            $this->joinOpen = $this->db->get("join");
        else
            $this->joinOpen = "closed";   
        if($this->db->get('winners') != "false")
            $this->winners = $this->db->get("winners");
        else
            $this->winners = array();
    }

    public function startBot()
    {
        $this->client->on('message', function ($data){
            $this->channel = $this->client->getChannelById($this->channelId)->get();
            $this->parseMessage($data);
        });
        $this->loop->run();
    }

    public function parseMessage($data)
    {
        if($data["text"] == $this->tagBot()." commands")
        {
            $msg = "Current commands are:";
            $msg .= "\n- tourney start [starts the tourney]";
            $msg .= "\n- tourney status [status of the tourney]";
            $msg .= "\n- tourney admin [admin of the tourney]";
            $msg .= "\n- join [join the current tourney]";
            $msg .= "\n- list players [displays the current tourney players]";
            $msg .= "\n- shuffle teams [creates the game schedule][admin only]";
            $msg .= "\n- schedule [displays the current tourney schedule]";
            $msg .= "\n- leaderboard [displays the leaderboard]";
            $msg .= "\n- winner game x team y [sets the winner of game number x to team number y][admin only]";
            $msg .= "\n- tourney winners [displays the all time tourney winners]";
            $msg .= "\n- tourney cancel [cancels the active tourney][admin only]";
            $message = $this->client->getMessageBuilder()
                ->setText($msg)
                ->setChannel($this->channel)
                ->create();
            $this->client->postMessage($message);
        }
        if($data["text"] == $this->tagBot()." tourney status")
        {
            $message = $this->client->getMessageBuilder()
                ->setText("Tourney is currently ".$this->tourneyStatus.".")
                ->setChannel($this->channel)
                ->create();
            $this->client->postMessage($message);
        }
        if($data["text"] == $this->tagBot()." tourney admin")
        {
            if($this->tourneyStatus == "active")
            {
                $message = $this->client->getMessageBuilder()
                ->setText("Current tourney admin is ".$this->tagUser($this->tourneyAdmin["id"]))
                ->setChannel($this->channel)
                ->create();
            $this->client->postMessage($message);
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is not active.")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            }
        }
        if($data["text"] == $this->tagBot()." tourney start")
        {
            if($this->tourneyStatus == "inactive")
            {
                $user = $this->client->getUserByID($data['user']);
                $this->tourneyAdmin = array("user" => $user->get()->getUsername(), "id" => $user->get()->getId());
                $this->tourneyStatus = "active";
                $this->joinOpen = "open";
                $this->db->set('admin', $this->tourneyAdmin);
                $this->db->set('status', $this->tourneyStatus);
                $this->db->set('join', $this->joinOpen);
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney started by ".$this->tagUser($this->tourneyAdmin["id"]).". Join-ins are open!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is already in progress!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            } 
        }
        if($data["text"] == $this->tagBot()." join")
        {
            if($this->tourneyStatus == "active" && $this->joinOpen == "open")
            {
                $alreadyIn = false;
                $user = $this->client->getUserByID($data['user']);
                $player = array("user" => $user->get()->getUsername(), "id" => $user->get()->getId());
                foreach($this->players as $p)
                {
                    if($player["id"] == $p["id"])
                        $alreadyIn = true;
                }
                if($alreadyIn == false)
                {
                    array_push($this->players, $player);
                    $this->db->set('players', $this->players);
                    $message = $this->client->getMessageBuilder()
                        ->setText($this->tagUser($player["id"])." joined the tourney!")
                        ->setChannel($this->channel)
                        ->create();
                    $this->client->postMessage($message);
                }
            }
            else
            {
                if($this->tourneyStatus == "inactive")
                {
                    $message = $this->client->getMessageBuilder()
                        ->setText("Tourney is not active!")
                        ->setChannel($this->channel)
                        ->create();
                    $this->client->postMessage($message);
                }
                if($this->joinOpen == "closed")
                {
                    $message = $this->client->getMessageBuilder()
                    ->setText("Join-ins are closed!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
                }
                
            } 
        }
        if($data["text"] == $this->tagBot()." list players")
        {
            if($this->tourneyStatus == "active")
            {
                $msg = "Current tourney players are: ";
                for($i = 0; $i < count($this->players); $i++)
                {
                    if($i == 0 && count($this->players) == 1)
                        $msg .= $this->tagUser($this->players[$i]["id"]).".";
                    elseif(count($this->players) > 1 && $i != count($this->players) - 1)
                        $msg .= $this->tagUser($this->players[$i]["id"]).", ";
                    elseif(count($this->players) > 1 && $i == count($this->players) - 1)
                        $msg .= $this->tagUser($this->players[$i]["id"]).".";
                }
                $msg .= "\nTotal players: ".count($this->players);   
                $message = $this->client->getMessageBuilder()
                    ->setText($msg)
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is not active!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            } 
        }
        if($data["text"] == $this->tagBot()." shuffle teams")
        {
            if($this->tourneyStatus == "active")
            {
                $user = $this->client->getUserByID($data['user']);
                $sender = array("user" => $user->get()->getUsername(), "id" => $user->get()->getId());
                if($sender["id"] == $this->tourneyAdmin["id"])
                {
                    if(count($this->players) % 2 == 0 && count($this->players) > 2)
                    {
                        $this->joinOpen = "closed";
                        $this->db->set('join', $this->joinOpen);
                        $chunk = array_chunk($this->players, 2);
                        shuffle($chunk);
                        for ($i = 0; $i < count($chunk); $i++)
                        {
                            $players = array();
                            $players = $chunk[$i];
                            $this->teams[$i]["players"] = $players;
                            $this->teams[$i]["id"] = substr($this->teams[$i]["players"][0]["user"], 0, 2).substr($this->teams[$i]["players"][1]["user"], 0, 2);
                        }
                        for ($i = 0, $max = count($this->teams); $i < $max; $i++)
                        {
                            for($j = $i + 1; $j < $max; $j++)
                            {
                                $this->games[] = array("team1" => $this->teams[$i], "team2" => $this->teams[$j], "winner" => "pending");
                            }
                        }
                        shuffle($this->games);
                        for ($i = 0; $i < count($this->teams); $i++)
                        {
                            $this->teams[$i]["w"] = 0;
                            $this->teams[$i]["l"] = 0;
                            $this->teams[$i]["p"] = 0;
                        }
                        $this->db->set('teams', $this->teams);
                        $this->db->set('games', $this->games);
                        $message = $this->client->getMessageBuilder()
                            ->setText("Teams are shuffled, join-ins are closed!")
                            ->setChannel($this->channel)
                            ->create();
                        $this->client->postMessage($message);
                    }
                    else
                    {
                        $message = $this->client->getMessageBuilder()
                            ->setText("Current number of players is not even or not enough players (".count($this->players).")!")
                            ->setChannel($this->channel)
                            ->create();
                        $this->client->postMessage($message);
                    }
                }
                else
                {
                    $message = $this->client->getMessageBuilder()
                        ->setText("Teams can only be shuffled by the tourney admin (".$this->tagUser($this->tourneyAdmin["id"]).")!")
                        ->setChannel($this->channel)
                        ->create();
                    $this->client->postMessage($message);
                }
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is not active!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            } 
        }
        if($data["text"] == $this->tagBot()." schedule")
        {
            if($this->tourneyStatus == "active")
            {
                $msg = "Tourney schedule:";
                for($i = 0; $i < count($this->games); $i++)
                {
                    $msg .= "\n".($i+1).". ".$this->games[$i]["team1"]["id"]." (".$this->tagUser($this->games[$i]["team1"]["players"][0]["id"]).", ".$this->tagUser($this->games[$i]["team1"]["players"][1]["id"]).") vs ".$this->games[$i]["team2"]["id"]." (".$this->tagUser($this->games[$i]["team2"]["players"][0]["id"]).", ".$this->tagUser($this->games[$i]["team2"]["players"][1]["id"]).") - Winner: ".$this->games[$i]["winner"];
                }
                $message = $this->client->getMessageBuilder()
                    ->setText($msg)
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is not active!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            } 
        }
        if($data["text"] == $this->tagBot()." leaderboard")
        {
            if($this->tourneyStatus == "active")
            {
                $leaderboard = $this->sortArray($this->teams, 'w');
                $msg = "Current leaderboard:";
                for($i = 0; $i < count($leaderboard); $i++)
                {
                    $msg .= "\n".($i+1).". ".$leaderboard[$i]["id"]." (".$this->tagUser($leaderboard[$i]["players"][0]["id"]).", ".$this->tagUser($leaderboard[$i]["players"][1]["id"]).") - Won: ".$leaderboard[$i]["w"]." Lost: ".$leaderboard[$i]["l"]." Played: ".$leaderboard[$i]["p"];
                }
                $message = $this->client->getMessageBuilder()
                    ->setText($msg)
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is not active!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            } 
        }
        if($this->startsWith($data["text"], $this->tagBot()." winner "))
        {
            if($this->tourneyStatus == "active")
            {
                $user = $this->client->getUserByID($data['user']);
                $sender = array("user" => $user->get()->getUsername(), "id" => $user->get()->getId());
                if($sender["id"] == $this->tourneyAdmin["id"])
                {
                    $command = explode(" ", $data["text"]);
                    if(count($command) == 6)
                    {
                        if($command[1] == "winner" && $command[2] == "game" && is_numeric($command[3]) && intval($command[3]) > 0 && intval($command[3]) <= count($this->games) && $command[4] == "team" && is_numeric($command[5]) && ($command[5] == "1" || $command[5] == "2"))
                        {
                            $pendingGames = array();
                            $counter = 0;
                            $game = $command[3];
                            $team = $command[5];
                            for($i = 0; $i < count($this->games); $i++)
                            {
                                if($this->games[$i]["winner"] == "pending")
                                {
                                    array_push($pendingGames, $i);
                                    $counter += 1;
                                }
                            }
                            if($counter > 0)
                            {
                                if(in_array($game-1, $pendingGames))
                                {
                                    $winner = "team".$team;
                                    if($winner == "team1")
                                        $loser = "team2";
                                    else
                                        $loser = "team1";
                                    $idWin = $this->games[$game-1][$winner]["id"];
                                    $idLose = $this->games[$game-1][$loser]["id"];
                                    $this->games[$game-1]["winner"] = $idWin;
                                    for($i = 0; $i < count($this->teams); $i++)
                                    {
                                        if($this->teams[$i]["id"] == $idWin)
                                        {
                                            $this->teams[$i]["w"] = $this->teams[$i]["w"] + 1;
                                            $this->teams[$i]["p"] = $this->teams[$i]["p"] + 1;
                                            break;
                                        }
                                    }
                                    for($i = 0; $i < count($this->teams); $i++)
                                    {
                                        if($this->teams[$i]["id"] == $idLose)
                                        {
                                            $this->teams[$i]["l"] = $this->teams[$i]["l"] + 1;
                                            $this->teams[$i]["p"] = $this->teams[$i]["p"] + 1;
                                            break;
                                        }
                                    }
                                    $this->db->set('teams', $this->teams);
                                    $this->db->set('games', $this->games);
                                    $message = $this->client->getMessageBuilder()
                                        ->setText("Winner of the game ".$game." is team ".$idWin."!")
                                        ->setChannel($this->channel)
                                        ->create();
                                    $this->client->postMessage($message);
                                    $calcPending = 0;
                                    for($i = 0; $i < count($this->games); $i++)
                                    {
                                        if($this->games[$i]["winner"] == "pending")
                                        {
                                            $calcPending += 1;
                                        }
                                    }
                                    if($calcPending == 0)
                                    {
                                        $leaderboard = $this->sortArray($this->teams, 'w');
                                        $msg = "Current leaderboard:";
                                        for($i = 0; $i < count($leaderboard); $i++)
                                        {
                                            $msg .= "\n".($i+1).". ".$leaderboard[$i]["id"]." (".$this->tagUser($leaderboard[$i]["players"][0]["id"]).", ".$this->tagUser($leaderboard[$i]["players"][1]["id"]).") - Won: ".$leaderboard[$i]["w"]." Lost: ".$leaderboard[$i]["l"]." Played: ".$leaderboard[$i]["p"];
                                        }
                                        $message = $this->client->getMessageBuilder()
                                            ->setText($msg)
                                            ->setChannel($this->channel)
                                            ->create();
                                        $this->client->postMessage($message);
                                        $message = $this->client->getMessageBuilder()
                                            ->setText("Tourney has finished!!!")
                                            ->setChannel($this->channel)
                                            ->create();
                                        $this->client->postMessage($message);
                                        $winners = array("id" => $leaderboard[0]["id"], "player1" => $leaderboard[0]["players"][0], "player2" => $leaderboard[0]["players"][1], "stats" => array("w" => $leaderboard[0]["w"], "l" => $leaderboard[0]["l"], "p" => $leaderboard[0]["p"]), "date" => date("d.m.Y"));
                                        array_push($this->winners, $winners);
                                        $this->db->set('winners', $this->winners);
                                        $this->players = array();
                                        $this->teams = array();
                                        $this->tourneyStatus = "inactive";
                                        $this->tourneyAdmin = array();
                                        $this->games = array();
                                        $this->joinOpen = "closed";
                                        $this->db->delete('players');
                                        $this->db->delete('teams');
                                        $this->db->delete('status');
                                        $this->db->delete('admin');
                                        $this->db->delete('games');
                                        $this->db->delete('join');
                                        $msg2 = "All time tourney winners:";
                                        for($i = 0; $i < count($this->winners); $i++)
                                        {
                                            $msg2 .= "\n".($i+1).". ".$this->winners[$i]["id"]." (".$this->tagUser($this->winners[$i]["player1"]["id"]).", ".$this->tagUser($this->winners[$i]["player2"]["id"]).") - Won: ".$this->winners[$i]["stats"]["w"]." Lost: ".$this->winners[$i]["stats"]["l"]." Played: ".$this->winners[$i]["stats"]["p"]." - Date: ".$this->winners[$i]["date"];
                                        }
                                        $message = $this->client->getMessageBuilder()
                                            ->setText($msg2)
                                            ->setChannel($this->channel)
                                            ->create();
                                        $this->client->postMessage($message);
                                    }
                                }
                                else
                                {
                                    $message = $this->client->getMessageBuilder()
                                        ->setText("Selected game (".$game.") is already finished!")
                                        ->setChannel($this->channel)
                                        ->create();
                                    $this->client->postMessage($message);
                                }
                            }
                            else
                            {
                                $message = $this->client->getMessageBuilder()
                                    ->setText("Tourney has finished!")
                                    ->setChannel($this->channel)
                                    ->create();
                                $this->client->postMessage($message);
                            }
                        }
                        else
                        {
                            $message = $this->client->getMessageBuilder()
                                ->setText("Invalid parameters detected!")
                                ->setChannel($this->channel)
                                ->create();
                            $this->client->postMessage($message);
                        }
                    }
                    else
                    {
                        $message = $this->client->getMessageBuilder()
                            ->setText("Invalid number of parameters!")
                            ->setChannel($this->channel)
                            ->create();
                        $this->client->postMessage($message);
                    }
                }
                else
                {
                    $message = $this->client->getMessageBuilder()
                        ->setText("Wins can only be assigned by the tourney admin (".$this->tagUser($this->tourneyAdmin["id"]).")!")
                        ->setChannel($this->channel)
                        ->create();
                    $this->client->postMessage($message);
                }
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is not active!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            } 
        }
        if($data["text"] == $this->tagBot()." tourney winners")
        {
            if(count($this->winners) > 0)
            {
                $msg = "All time tourney winners:";
                for($i = 0; $i < count($this->winners); $i++)
                {
                    $msg .= "\n".($i+1).". ".$this->winners[$i]["id"]." (".$this->tagUser($this->winners[$i]["player1"]["id"]).", ".$this->tagUser($this->winners[$i]["player2"]["id"]).") - Won: ".$this->winners[$i]["stats"]["w"]." Lost: ".$this->winners[$i]["stats"]["l"]." Played: ".$this->winners[$i]["stats"]["p"]." - Date: ".$this->winners[$i]["date"];
                }
                $message = $this->client->getMessageBuilder()
                    ->setText($msg)
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("There are currently no tourney winners.")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            }
        }
        if($data["text"] == $this->tagBot()." tourney cancel")
        {
            if($this->tourneyStatus == "active")
            {
                $user = $this->client->getUserByID($data['user']);
                $sender = array("user" => $user->get()->getUsername(), "id" => $user->get()->getId());
                if($sender["id"] == $this->tourneyAdmin["id"])
                {
                    $this->players = array();
                    $this->teams = array();
                    $this->tourneyStatus = "inactive";
                    $this->tourneyAdmin = array();
                    $this->games = array();
                    $this->joinOpen = "closed";
                    $this->db->delete('players');
                    $this->db->delete('teams');
                    $this->db->delete('status');
                    $this->db->delete('admin');
                    $this->db->delete('games');
                    $this->db->delete('join');
                    $message = $this->client->getMessageBuilder()
                        ->setText("Tourney is canceled!")
                        ->setChannel($this->channel)
                        ->create();
                    $this->client->postMessage($message);
                }
                else
                {
                    $message = $this->client->getMessageBuilder()
                        ->setText("Tourney can only be caceled by the tourney admin (".$this->tagUser($this->tourneyAdmin["id"]).")!")
                        ->setChannel($this->channel)
                        ->create();
                    $this->client->postMessage($message);
                }
            }
            else
            {
                $message = $this->client->getMessageBuilder()
                    ->setText("Tourney is not active!")
                    ->setChannel($this->channel)
                    ->create();
                $this->client->postMessage($message);
            } 
        }
    }

    public function tagBot()
    {
        return "<@".$this->botId.">";
    }

    public function tagUser($id)
    {
        return "<@".$id.">";
    }

    public function sortArray($arr, $col, $dir = SORT_DESC) {
        $sortCol = array();
        foreach ($arr as $key => $row) {
            $sortCol[$key] = $row[$col];
        }
        array_multisort($sortCol, $dir, $arr);
        return $arr;
    }

    public function startsWith($string, $word)
    {
         $length = strlen($word);
         return (substr($string, 0, $length) === $word);
    }

}